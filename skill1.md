# Browser Rendering

## introduction

Whenever you are developing a website, certain things are very essential for a good user experience. Some of the common problems a website may encounter could be slow loading of the resources, waiting for unnecessary files to download on initial render, a flash of unstyled content (FOUC), etc. To avoid such problems, we need to understand the lifecycle of how a browser renders a typical webpage.

# How does a browser render HTML, CSS, and JS to DOM? What is the mechanism behind it?

## Browsing HTML 

First, we need to understand what DOM is. When a browser sends a request to a server to fetch an HTML document, the server returns an HTML page in binary stream format which is a text file with the response header Content-Type set to the value text/html; charset=UTF-8.




#### An example of an HTML file and its corresponding DOM is shown below:
```HTML
  <html>
  <head>
  <title>Rendering Test</title>
  
  <!-- stylesheet -->
  <link rel="stylesheet" href="./style.css"/>
  </head>
  <body>
  <div class="container">
  <h1>Hello World!</h1>
  <p>This is a sample paragraph.</p>
  </div>
  
  <!-- script -->
  <script src="./main.js"></script>
  </body>
  </html>
```
### Behind the mechanism


Standard Generalized Mark-up Language
The HTML that Tim invented was strongly based on SGML (Standard Generalized Mark-up Language), an internationally agreed-upon method for marking up text into structural units such as paragraphs, headings, list items, and so on. SGML could be implemented on any machine.

- his is a link in HTML. To create a link the <a> tag is used. The href attribute holds the URL address of the link. 
- <a href="https://www.wikipedia.org/">A link to Wikipedia!</a>



## CSSOM Rendering
 

 * CSS Object Model (CSSOM)

CSSOM stands for CSS object model. when the rendering engine is constructing the dom from the HTML file it encounters a link tag referring to the CSS stylesheet, Once the construction of the Dom tree is completed, CSS is mentioned in all the sources i.e external, inline,user-agent, etc. is read by the engine.

#### An example of a CSS file and its corresponding DOM is shown below:
```HTML
<html>
  <head>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <link href="style.css" rel="stylesheet" />
    <title>Critical Path</title>
  </head>
  <body>
    <p>Hello <span>web performance</span> students!</p>
    <div><img src="awesome-photo.jpg" /></div>
  </body>
</html>
```

### Behind the mechanism

To make it more concrete, consider the CSSOM tree above. Any text contained within the <span> tag that is placed within the body element, has a font size of 16 pixels and has red text—the font-size directive cascades down from the body to the span. However, if a span tag is a child of a paragraph (p) tag, then its contents are not displayed.


- ex:-


```
body {
  font-size: 16px;
}
p {
  font-weight: bold;
}
span {
  color: red;
}
p span {
  display: none;
}
img {
  float: right;
}
```


## JS rendering

*  Document Object Model or DOM is an interface that defines how the browser reads your XML or HTML document. JavaScript is allowed to manipulate the structure & style of your webpage. Once the browser reads the HTML document, it creates a representational tree known as the Document Object Model.


---What is JavaScript rendering?

Javascript uses the document object model (DOM) to manipulate the DOM elements. Rendering refers to showing the output in the browser.


### Rendering content with JavaScript 

The render() method #

React has a function called render() that lets you pass in a template and the element to render it into, and it handles the rest.

We can create something similar with just a few lines of vanilla JavaScript. First, let’s set up our render() method.

var render = function (template, node) {
  // Codes go here...
};

This follows the same structure as React, where you pass in a template and the node to render it into.

To render content, we’ll use innerHTML, a property that lets you set the inner HTML of an element.

var render = function (template, node) {
  node.innerHTML = template;
};

Now, we can render content like this:

var template = '<h1>Hello world!</h1>';
render(template, document.querySelector('#main'));


### Checking that the node exists #

One thing we should do is make sure a node exists before trying to set its innerHTML. Otherwise, we might throw an error.

We’ll simply check that node was provided and exists, and if not, return to end our function.

var render = function (template, node) {
  if (!node) return;
  node.innerHTML = template;
};



### Behind the mechanism


One of JS's essential concepts to understand is hoisting. But, before learning what it is, we need to learn first about the scope of JavaScript.
What's the scope of JavaScript?

Scope determines the accessibility of variables inside a JavaScript script.

There are different scopes in a JavaScript program:

    - Global Scope
    - Functional Scope
    - Block Scope

💡 Global scope

The global scope is known as global variables and functions. A global variable or function is accessible and used inside the whole JS script.

```javascript
let variable= "Global scope";
function scope() {
  let variable = "Local scope (inside a function)";
  console.log(variable); // displays "Local scope (inside a function)"
}
scope();
console.log(variable);// displays "Global scope"
```
💡 Functional scope

The functional scope is known as variables declared inside a function. In this case, the scope or the area where you can use/manipulate this variable is only inside that function and not outside. These variables are called local variables.


```javascript
function scope() {
  let variable = "Local scope (inside a function)";
  console.log(variable); // displays "Local scope (inside a function)"
}
scope();
console.log(variable);// variable is not defined
```
💡Block scope

A bit like functional scope, but in the block scope, the variables are declared inside a block {}. These variables are not accessible outside of the block:

{ // declared here? then accessible only here}

Example:
```javascript
{ 
  let name = "scope";
}
// The name variable is unknown here, you can not use it.

```
